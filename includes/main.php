<?php

// Подключаем дополнительные файлы
require __DIR__ . '/Query.class.php';
require __DIR__ . '/User.class.php';
require __DIR__ . '/functions.php';

// Создаем соединение
DB::config([
  'host' => $configs['host'],
  'user' => $configs['user'],
  'pass' => $configs['pass'],
  'name' => $configs['name'],
]);
$result = DB::select('SELECT * FROM `reg_users` WHERE id = ? AND token = ?', 1, 'qweqwceqwe823234n389248y23vn9482y3n4');
dd($result);

// Uncomment to keep people logged in for a week
// session_set_cookie_params(60 * 60 * 24 * 7);
session_start();

/**
 * Other settings
 */
// The 'from' email address that is used in the emails that are sent to users.
// Some hosting providers block outgoing email if this address
// is not registered as a real email account on their system, so put a real one here.

$fromEmail = '';

if (!$fromEmail) {
	// This is only used if you haven't filled an email address in $fromEmail
	$fromEmail = 'noreply@' . $_SERVER['SERVER_NAME'];
}
