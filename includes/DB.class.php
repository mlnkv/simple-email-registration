<?php

class DB {

  private static $connect;

  public static function config($params) {
    self::$connect = new mysqli($params['host'], $params['user'], $params['pass'], $params['name']);
  }

  private function interpolate($sql) {

    $args = array_slice(func_get_args(), 1);
    $argn = count($args);
    $stmt = mysqli_prepare(self::$connect, (string) $sql);

    if ($stmt === false) {
      throw new mysqli_sql_exception(mysqli_error(self::$connect), mysqli_errno(self::$connect));
    }

    if ($argn) {

      $syms = implode('', array_pad([], $argn, 's'));
      $refs = [];

      foreach ($args as $key => $val) {
        $refs[$key] = &$args[$key];
      }

      if (false === call_user_func_array('mysqli_stmt_bind_param', array_merge([$stmt, $syms], $refs))) {
        throw new mysqli_sql_exception(
          mysqli_stmt_error($stmt),
          mysqli_stmt_errno($stmt)
        );
      }
    }

    return $stmt;
  }

  # Executes a select and returns all resulting rows
  public static function select($sql) {

    $stmt = call_user_func_array('self::interpolate', func_get_args());

    if (!mysqli_stmt_execute($stmt) || (false === ($result = mysqli_stmt_get_result($stmt)))) {
      throw new mysqli_sql_exception(
        mysqli_stmt_error($stmt),
        mysqli_stmt_errno($stmt)
      );
    }

    $rows = mysqli_fetch_all($result, MYSQLI_ASSOC);

    mysqli_free_result($result);
    mysqli_stmt_close($stmt);

    return (array) $rows;
  }

  # Executes an insert and returns the insert id if any.
  public static function insert($db, $sql) {

    $stmt = call_user_func_array('self::interpolate', func_get_args());

    if (!mysqli_stmt_execute($stmt)) {
      throw new mysqli_sql_exception(
        mysqli_stmt_error($stmt),
        mysqli_stmt_errno($stmt)
      );
    }

    $id = mysqli_insert_id($db);
    mysqli_stmt_close($stmt);

    return (string) $id;
  }

  # Executes an update/delete query and returns affected row count.
  public static function update($db, $sql) {

    $stmt = call_user_func_array('self::interpolate', func_get_args());

    if (!mysqli_stmt_execute($stmt)) {
      throw new mysqli_sql_exception(
        mysqli_stmt_error($stmt),
        mysqli_stmt_errno($stmt)
      );
    }

    $affected = mysqli_stmt_affected_rows($stmt);
    mysqli_stmt_close($stmt);

    return (int) $affected;
  }
}

?>