<?php

error_reporting(0);

$error = null;
$message = null;

if ($_POST['host']) {

  // Создаем соединение
  $conn = new mysqli($_POST['host'], $_POST['user'], $_POST['pass']);

  // Прверяем соединение
  if ($conn->connect_error) {

    $error = 'Ошибка подключения к базе:<br>' . $conn->connect_error;
  } else {

    $sql = file_get_contents('db.sql');
    $sql = preg_replace('/%name%/', $_POST['name'], $sql);

    if ($conn->multi_query($sql)) {
      $config = array(
        'host' => $_POST['host'],
        'user' => $_POST['user'],
        'pass' => $_POST['pass'],
        'name' => $_POST['name']
      );

      $conf_string = '';
      foreach ($config as $key => $value) {
        $conf_string .= ($conf_string ? ",\n  " : "") . "'{$key}' => '{$value}'";
      }
      $conf_string = "<?php\n\n\$configs = array(\n\t{$conf_string}\n);\n\n?>";
      file_put_contents('configs.php', $conf_string);
      $message = 'База данных успешно создана.';

    } else {
      $error = 'Ошибка создания базы данных:<br>' . $conn->error;
    }

    $conn->close();
  }
}

?><!DOCTYPE html>
<html>

  <head>
    <meta charset="utf-8">
    <title>Tutorial: Super Simple Registration System With PHP &amp; MySQL</title>
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,700" rel="stylesheet">
    <link href="assets/css/style.css" rel="stylesheet">
  </head>

  <body>
   <?php if ($error || $message) { ?>

    <?php if ($error) { ?>
      <div class="banner banner-error">
        <p><?php echo $error ?></p>
        <a href="">Попробовать еще раз</a>
      </div>
    <?php } ?>

    <?php if ($message) { ?>
      <div class="banner banner-success">
        <p><?php echo $message ?></p>
        <a href="./">На главную</a>
      </div>
    <?php } ?>

  <?php } else { ?>

    <form id="install" class="loggedIn" method="post">
      <h1>Установка</h1>
      <h3>Параметры подключения к базе данных</h3>

      <label class="c-label">
        Хост базы данных
        <input class="c-input" name="host" value="localhost" required>
      </label>
      <label class="c-label">
        Пользователь базы данных
        <input class="c-input" name="user" value="root" required>
      </label>
      <label class="c-label">
        Пароль базы данных
        <input class="c-input" name="pass" value="">
      </label>
      <label class="c-label">
        Имя базы данных
        <input class="c-input" name="name" value="simple_email_registration" required>
      </label>

      <button>Установка</button>
    </form>

  <?php } ?>

  </body>
</html>